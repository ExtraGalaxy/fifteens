# Fifteens
Генерирует поле для игры в пятнашки в собранном состоянии. При нажатии на ячейку с номером проверяется, есть ли рядом с ней пустая клетка и, если это условие выполнено, то ячейка перемещается на пустую.
# TwoCelestialObject
При вызове функции time_analysis() выводит график величины погрешности численного интегрирования с течением времени за k = 1000 периодов для спутника, вращающегося вокруг Земли на высоте 8000 км, со скоростью в перицентре порядка 8.64 км/с

При вызове функции step_analysis() выводит график величины погрешности численного интегрирования за один период от величины шага для спутника, вращающегося вокруг Земли на высоте 8000 км, со скоростью в перицентре порядка 8.64 км/с
