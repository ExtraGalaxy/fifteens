# FLASK
from tkinter import *

size = 601
root = Tk()
f = 15
def generate_possible(table):
	global pos
	x = table[0]
	y = table[1]
	pos = []
	if x - 1 >= 0:
		pos += [[x-1, y]]
	else:
		pos += [[-1]]
	
	if y + 1 < 4:
		pos += [[x,y+1]]
	else:
		pos += [[-1]]
	
	if x + 1 < 4:
		pos += [[x+1, y]]
	else:
		pos += [[-1]]

	if y - 1 >= 0:
		pos += [[x, y-1]]
	else:
		pos += [[-1]]

def dictionary(button, text, buttoncoordinate, txtcoordinate, table):
	global dic, cell
	cell = []
	dic = dict()
	for i in range(16):
		dic[str(i)] = [button[i], text[i], buttoncoordinate[i], txtcoordinate[i], table[i%4][i//4]]
		cell += [dic[str(i)]]	
		
def Tab():
	global table, pos
	table = [[[0 for j in range(2)] for i in range(4)] for k in range(4)]
	for i in range(4):
		for j in range(4):
			table[i][j] = [i,j]
	pos = [[2,3], [3,2], [-1], [-1]]

def create_15():
	global button, text, buttoncoordinate, txtcoordinate
	button = []
	text = []
	buttoncoordinate = []
	txtcoordinate = []
	for i in range(15):
		button += [canvas.create_rectangle(size//4 * (i%4), size//4*(i//4), size//4 * (i%4 + 1), size//4*(i//4 + 1), fill = "indigo", outline = "white")]
		text += [canvas.create_text(size//4 * (i%4) + size//8, size//4 * (i//4) + size//8, text = str(i + 1), font = "Arial 50", fill = "chartreuse")]
		buttoncoordinate += [[size//4 * (i%4), size//4*(i//4), size//4 * (i%4 + 1), size//4*(i//4 + 1)]]
		txtcoordinate += [[size//4 * (i%4) + size//8, size//4 * (i//4) + size//8]]
	button += [canvas.create_rectangle(size//4 * (15%4), size//4*(15//4), size//4 * (15%4 + 1), size//4*(15//4 + 1), fill = "white")]
	text +=  [canvas.create_text(size//4 * (15%4) + size//8, size//4 * (15//4) + size//8, font = "Arial 50", fill = "chartreuse")]
	buttoncoordinate += [ [size//4*(15%4), size//4*(15//4), size//4 * (15%4+1), size//4*(15//4 + 1)] ]
	txtcoordinate += [ [size//4 * (15%4) + size//8, size//4 * (15//4) + size//8] ]


def create_table(N, size):
	line = []
	for i in range(N):
    		canvas.create_line(size//(N - 1) * i + 1, 1, size//(N-1) * i + 1, size)
	for i in range(N):
    		canvas.create_line(0 + 1, size//(N-1) * i + 1, size, size//(N-1) * i + 1)
	

def possible(event):
	global f
	x = event.x
	y = event.y
	for i in range(4):
		if size//4 * (i%4) <= x and size//4 * (i%4 + 1) >= x:
			break
	x = i
	for i in range(4):
		if size//4*(i) <= y and y <= size//4*(i + 1):
			break	
	y = i
	k = x + y*4
	for i in range(4):
		if pos[i][0] != -1:
			if dic[str(k)][4] == pos[i]:
				dic[str(f)][0], dic[str(k)][0] = dic[str(k)][0], dic[str(f)][0]
				dic[str(f)][1], dic[str(k)][1] = dic[str(k)][1], dic[str(f)][1]				
				canvas.coords(dic[str(k)][0], *dic[str(k)][2])
				canvas.coords(dic[str(k)][1], *dic[str(k)][3])
				canvas.coords(dic[str(f)][0], *dic[str(f)][2])
				canvas.coords(dic[str(f)][1], *dic[str(f)][3])
				f = k				
				break
	generate_possible(dic[str(f)][4])
	root.update()
	

canvas = Canvas(width=size,height=size,bg = "white")
canvas.pack()
create_table(5, size)
create_15()
Tab()
dictionary(button, text, buttoncoordinate, txtcoordinate, table)

root.bind("<Button-1>", possible)
root.mainloop()
    
