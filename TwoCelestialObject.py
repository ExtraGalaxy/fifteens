import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(precision = 16)

def f(vec):
    const = 398601.3
    x = vec[:2]
    p = vec[2:]
    dp = - (const * x /np.sum(x**2)**1.5)
    dx = p
    vecn = np.hstack((dx, dp))
    return vecn

def RungeKutta(vec, t, f, h):
    p = f(vec)
    q = f(vec + .5*h*p)
    r = f(vec + h/2*q)
    s = f(vec + h*r)
    vecn = vec + 1/6*h*(p + 2*q + 2*r + s)
    return vecn

def step_analysis():
    result = []; T = 20141.43860897035; n = 16
    i = 24
    for k in range(1,i):
        vec = np.array([0, 8000., 8.645099406600250, 0])
        n = 2**k
        timepoints = np.linspace(0, T*(1), n, endpoint = False)
        step = timepoints[1] - timepoints[0]
        for t in np.linspace(0, T*(1), n, endpoint = False):
            vec = RungeKutta(vec, t, f, h = step)
        res = ((vec[0])**2 + (vec[1] - 8000)**2)**.5
        result += [res]
        
    
    plt.plot(T/2**np.arange(1,i), result, 'ks')
    plt.xscale('log')
    plt.yscale('log')
    plt.title("График зависимости погрешности\nчисленного интегрирования от величины шага")
    plt.xlabel('Шаг интегрирования, с')
    plt.ylabel('Ошибка, км')
    plt.savefig('3.jpg')
    plt.show()


def time_analysis():
    vec = np.array([0, 8000., 8.645099406600250, 0])
    print(vec)
    result = []; T = 20141.43860897035; n = 16
    k = 1000
    for _ in range(1,k):    
        n = 2**12
        timepoints = np.linspace(0, T*(1), n, endpoint = False)
        step = timepoints[1] - timepoints[0]

        for t in np.linspace(0, T*(1), n, endpoint = False): 
            vec = RungeKutta(vec, t, f, h = step)
        res = ((vec[0])**2 + (vec[1] - 8000.)**2)**.5
        result += [res]
        
    
    plt.plot(np.arange(1, k), result, 'ks')
    plt.title("График величины погрешности\n численного интегрирования c течением времени.")
    plt.xlabel('Время в периодах')
    plt.ylabel('Ошибка, км')
    plt.savefig('4.jpg')
    plt.show()

if __name__ == '__main__':
    time_analysis()
